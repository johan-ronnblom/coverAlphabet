import numpy
import argparse

class CoverAlphabet:
    def __init__(self):
        parser = argparse.ArgumentParser(description='In a dictionary, find maximal sets of equal length words that share no characters.')
        parser.add_argument('dictionary', type=str, help='dictionary filename')
        parser.add_argument('wordlength', type=int, nargs='?', help='length of words to find', default=5)
        args = parser.parse_args()
        self.wordLength = args.wordlength
        self.prepare(args.dictionary)
        if len(self.wordsByHash) > 0:
            self.find(len(self.alphabet) - 1)

    def readWords(self, filename):
        wordsByAnagram = {}
        with open(filename) as file:
            while (word := file.readline().rstrip()):
                if self.wordLength == len(word) == len(set(word)):
                    sortedWord = ''.join(sorted(list(word)))
                    wordsByAnagram.setdefault(sortedWord, [])
                    wordsByAnagram[sortedWord].append(word)
        return wordsByAnagram

    def createAlphabet(self, words):
        counts = {}
        for word in words:
            for c in word:
                counts[c] = counts.setdefault(c, 0) + 1
        self.alphabet = ''.join(sorted(counts.keys(), key=lambda l: -counts[l]))

    def hash(self, word):
        hash = 0
        for c in word:
            hash |= 1 << self.alphabet.index(c)
        return hash

    def prepare(self, filename):
        wordsByAnagram = self.readWords(filename)
        self.createAlphabet(wordsByAnagram)
        self.wordCount = int(len(self.alphabet) / self.wordLength)
        self.found = [0] * self.wordCount
        words = dict(sorted(wordsByAnagram.items(), key=lambda w: -self.hash(w[0])))
        self.wordsByHash = {}
        blocks = [[] for i in range(len(self.alphabet))]
        msb = len(self.alphabet)
        self.unusedChars = len(self.alphabet) % self.wordLength
        for word in words:
            h = self.hash(word)
            msb -= not bool(h & (1 << msb))
            self.wordsByHash[h] = words[word]
            blocks[msb].append(h)
        self.blocks = list(map(lambda b: numpy.array(b), blocks))

    def printFoundWords(self):
        r = []
        for h in self.found:
            r.append('/'.join(self.wordsByHash[h]))
        print(' '.join(r))

    def find(self, block, depth = 0, totalHash = 0, missingChars = 0):
        while totalHash & (1 << block):
            block -= 1
        b = self.blocks[block]
        if len(b) == 0: return
        for h in b[(b & totalHash) == 0]:
            self.found[depth] = h
            if depth == self.wordCount - 1:
                self.printFoundWords()
            else:
                self.find(block - 1, depth + 1, totalHash | h, missingChars)
        if missingChars < self.unusedChars:
            self.find(block - 1, depth, totalHash, missingChars + 1)

CoverAlphabet()
