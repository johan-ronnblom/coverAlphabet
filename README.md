# Cover Alphabet

By Johan Rönnblom

A solution to the problem of finding all combinations of N letter words in a
dictionary, such that no words share a letter and there are not enough unused
letters in the dictionary alphabet to form one more word.

## Description

On his YouTube channel Stand-up Maths, Matt Parker highlighted this problem for
the case of five letter words and the English 26 letter alphabet. He solved the
problem with an algorithm that he reports took a month on his laptop.
https://youtu.be/_-AfhLQfb6w

Benjamin Paassen improved on this using a graph theory algorithm that calculates
'vertexes' between words and finds a 'five clique' of such vertexes. This is a
fancy-schmanzy way of saying that for every five-letter word in the dictionary,
he made a list of all the words that don't share any letters with it. This is
typically most other words in the dictionary, and the generated lists take about
90 megabytes (!) of disk space.

Then, all such lists are compared against other lists to find instances of five
lists that refer to each other (the 'five clique'). All in all, this reportedly
took about 22 minutes on his computer. My laptop is not that fast, here it seems
to take about 29 minutes in total.

This appeared to the author as optimizable.

# Algorithm

It is noted that for the English case and five words with five letters, exactly
one letter remains unused. This means that an exhaustive search can be aborted
as soon as there are two letters proven to be unused. Moreover, it is noted that
characters in the English dictionary are very unevenly distributed, with the
letter Q being much less common than the letter A. More generally, for K
characters and N letter words, no more than K mod N letters may be unused.

This solution groups all five letter anagrams, then splits them into blocks such
that the first block contains all instances of the rarest character (Q), the
second block contains all remaining instances of the second rarest character,
and so on.

We can then find all valid word combinations by searching the first available
block for all words that don't have any previously used character (eg all of
them), and then recurse to find a second word, and so on until we have five
words. At every recursion, we can skip any blocks we have already searched, or
where all words have an already used character. For instance, if the first word
is 'SQUIZ', there is no need to search the block where all words contain a Z.

When at any point we have exhausted a block, and thus proven that the common
letter of this block can not be used (*), we proceed with the next available
block, but only if we have not already previously skipped more than K mod N
blocks (eg for English and 5 words, only 1 skip is allowed).

(*) if any previously selected word uses that character, we would already have
skipped that block.

# Implementation

Algorists frown on proportional-time improvements. Nevertheless, implementations
can vary in efficiency by several orders of magnitude.

It is noted that by definition, the words we are interested in have no repeat
characters, and that we do not care about the order of the characters. We can
therefore represent each word as a bit pattern, where each bit position
represents a character in the dictionary. For instance, if our alphabet is
'aesiorunltycdhmpgkbwfvzjxq' we can write the word ARISE as 111101 while AROSE
becomes 111011.

We note that this allows us to see if two words share any character by
performing a logical AND on their bit representations and checking if the result
is zero. Indeed, we can compare a word against the union of several other words
in the same way.

The popular Python module Numpy is used to quickly perform this operation over
an entire block of words.

This allows the search to complete in less than 2 seconds with no significant
use of memory, rather than the twenty-something minutes and ninety megabytes for
fancy-schmanzy. But he gave it a go. And that's something.

## Usage

The 'words_alpha.txt' file from https://github.com/dwyl/english-words has been
used for comparisons.

Run the program with
 python3 coverAlphabet.py words_alpha.txt 5

to search that dictionary file for five letter words, or try another number to
explore other word lengths (10 is interesting).
